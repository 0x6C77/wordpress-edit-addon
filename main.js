(() => {
    var getEditURL = () => {
        var exp = /\S-(\d+)/,
            classes = document.body.className,
            ID = exp.exec(classes)[1];

        if (ID) {
            return '/wp-admin/post.php?action=edit&post=' + ID;
        } else {
            return false;
        }
    };

    var checkWordpress = (callback) => {
        var baseURL = location.protocol+'//'+location.hostname;

        var ajaxURL = baseURL + "/wp-admin/admin-ajax.php";
        var params = "action=heartbeat";
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var data = xhr.responseText;
            data = JSON.parse(data);

            if (data["wp-auth-check"] !== false) {
                callback();
            }
        }

        xhr.open("POST", ajaxURL, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(params);
    }

    // Check we are on an editable page
    if (getEditURL()) {
        checkWordpress(() => {
            // Add new HTML elements to page
            var edit = document.createElement("a");
            edit.classList.add("wordpress-edit-button");
            edit.text = "edit";
            edit.setAttribute("target", "_blank");
            edit.setAttribute("href", getEditURL());

            document.body.appendChild(edit);
        });
    }
})()